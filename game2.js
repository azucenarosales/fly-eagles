//set width and height variables for game
var width = 800;
var height = 600;
//create game object and initialize the canvas
var game = new Phaser.Game(width, height, Phaser.AUTO, null, {preload: preload, create: create, update: update});

var player;
var stars;
var cursors;
var speed = 450;
var score = 0;
var scoreText;
var touchdownArea = 400;
var largoCancha = 7930;
var anchoCancha = 738;
var posession = false;
var zonaPuntos = largoCancha - touchdownArea*2;
var zonaPuntosValida = 50;
var vidas = 3;
var bandera= true;

function preload() {
    game.load.image('ball', 'asset/red-square.png');    
    game.load.image('background','asset/stadium.png');
}

function create() {
	game.add.tileSprite(0, 0, largoCancha, anchoCancha, 'background');
	game.world.setBounds(0, 0, largoCancha, anchoCancha);
	//start arcade physics engine
	game.physics.startSystem(Phaser.Physics.ARCADE);	

    // ---------------------
    // Ball
    // ---------------------

    ball = game.add.sprite(largoCancha - touchdownArea, game.world.height/2, 'ball');

    ball.enableBody = true;    

    game.physics.enable(ball, Phaser.Physics.ARCADE);

	// ---------------------
    // Player 1
    // ---------------------

    var playerData = [
        '.....CCCCCC.....',
        '...CCCCCCCCCC...',
        '.CCCCCCCCCCCCCC.',
        'CCC00000000000CC',
        'CC0222222222210C',
        'C0222222222210CC',
        '0222222211110CCC',
        '222222110000CCCC',
        '....0100CCCCCCCC',
        '....00CCCCCCCCCC',
        '....0CCCCCCCCCCC',
        '....0CCCCCCCCCCC',
        '....0CCCCCCCCCCC',
        '..0000CCC00CCCCC',
        '000.0.00C00CCCC.',
        '0.0.0000CCCCCC..',
        '00000.CCCCCCC...'        
    ];
    game.create.texture('player', playerData, 3, 3, 0);

    //player = game.add.physicsGroup();

    //player = player.create(largoCancha - touchdownArea, game.world.height/2, 'player');
    player = game.add.sprite(largoCancha - touchdownArea, game.world.height/2, 'player');
    player.anchor.set(0.5);

    game.physics.arcade.enable(player);
    //  La camara sigue al jugador
    game.camera.follow(player);
    

    player.body.bounce.x = 0.7 + Math.random() * 0.2;

    // Modo Facil:
    //player.body.bounce.x = 0.5;
    player.body.collideWorldBounds = true;    

	// --------------
	// Enemigo
	// --------------	

    var cowboysData = [
        '.....222222.....',
        '...2211111122...',
        '..211111111112..',
        '.21111111111112.',
        '221111D111111112',
        '211111D111111111',
        '2111DDDDD11111..',
        '21111DDD111111..',
        '21111D1D1111....',
        '2111D111D111....',
        '211111111110....',
        '211111111110....',
        '211111111110....',
        '11111001100000..',
        '.111100100.0.000',
        '..1111110000.0.0',
        '...11111...00000'
    ];
    

    game.create.texture('cowboysTexture', cowboysData, 3, 3, 0);

    cowboys = game.add.physicsGroup();

    var y = 10;

    for (var i = 0; i < 35; i++) {
        var cowboy = cowboys.create(game.world.randomX, y, 'cowboysTexture');
        cowboy.body.velocity.x = game.rnd.between(100, 400);        
        y += 20;        
        cowboy.body.immovable = true;
        //cowboy.body.checkCollision.left = false;
        //cowboy.body.checkCollision.up = false;
       // cowboy.body.checkCollision.down = false;
       // cowboy.body.checkCollision.right = true;
    } 

    // ------------------------
    // Score
    // ------------------------
    scoreText = game.add.text(0, 0, "Score: " + score +  " Vidas: " + vidas);

    scoreText.fixedToCamera = true;
    scoreText.cameraOffset.setTo(10, 0);
        //inicializa las flechas del teclado como los controles
    cursors = game.input.keyboard.createCursorKeys();
}
function update() {

    if(vidas == -1){
        //player.x = largoCancha - touchdownArea;
        //player.y = game.world.height/2;
        vidas = 3;
    }
	cowboys.forEach(checkPos, this);

    //player.body.velocity.x = 0;
    player.body.velocity.y = 0;

    if (cursors.up.isDown) {
        player.body.velocity.y = -speed;
    } else if (cursors.down.isDown) {
        player.body.velocity.y = speed;
    }

    if (cursors.left.isDown) {
        player.body.velocity.x = -speed;
    } else if (cursors.right.isDown) {
        player.body.velocity.x = speed;
    }
    // El jugador toma la pelota
    game.physics.arcade.overlap(player, ball, takeBall);
    
    // El jugador choca con el enemigo
    if(player.body.touching.right == true){
        colision(player, cowboys);
    }
    
        

    if(player.x < touchdownArea) {        
        scoreText.text = 'Ganaste!!';    
    }
    scoreText.text = "Score: " + score +  " Vidas: " + vidas;

    if(Math.round(player.x) > zonaPuntos && Math.round(player.x) < zonaPuntos + zonaPuntosValida ) {                
        score = score + 10;
        zonaPuntos = zonaPuntos - touchdownArea;
        scoreText.text = "Score: " + score;
    }
    
    //scoreText.text = 'player x' + Math.round(player.x) + '\ntouchdownArea ' + touchdownArea + '\nzonaPuntos ' + zonaPuntos  + '\nscore: ' +  score;
}

function colision(player, enemy) {    
    vidas --;
    //player.body.velocity.x = -200;    
    
}

function takeBall(player, ball) {
   // player.body.velocity.x = -200;
    ball.kill();
    posession = true;
}

function checkPos (cowboy) {
    if (cowboy.x > largoCancha) {
        cowboy.x = -100;
    }    
}