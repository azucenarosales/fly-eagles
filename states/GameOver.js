var GameOver = function(game) {};

GameOver.prototype = {

  preload: function () {
    this.optionCount = 1;
  },

  addMenuOption: function(text, callback) {
    var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    var txt = game.add.text(game.world.centerX, (this.optionCount * 80) + 300, text, optionStyle);
    txt.anchor.setTo(0.5);
    txt.stroke = "rgba(0,0,0,0)";
    txt.strokeThickness = 4;

    txt.setShadow(1, 1, 'rgba(0,0,0,5)', 5);

    var onOver = function (target) {
      target.fill = "white";
      target.stroke = "black";
      txt.useHandCursor = true;
      txt.setShadow(1, 1, 'rgba(0,0,0,5)', 5);
    };
    var onOut = function (target) {
      target.fill = "white";
      target.stroke = "rgba(0,0,0,0)";
      txt.useHandCursor = false;
      txt.setShadow(1, 1, 'rgba(0,0,0,5)', 5);
    };
    txt.inputEnabled = true;
    txt.events.onInputUp.add(callback, this);
    txt.events.onInputOver.add(onOver, this);
    txt.events.onInputOut.add(onOut, this);
    this.optionCount ++;
  },

  create: function () {
    game.world.setBounds(0, 0, 800, 600);
    game.add.sprite(0, 0, 'gameover-bg');
    this.addMenuOption('Reiniciar!', function (e) {
      this.game.state.start("Game");
    });
    this.addMenuOption('Menu', function (e) {
      this.game.state.start("GameMenu");
    })
  }
};