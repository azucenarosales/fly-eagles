var Credits = function(game) {};

Credits.prototype = {

  preload: function () {
    this.optionCount = 1;
    this.creditCount = 0;

  },

  addCredit: function(task, author) {
    var authorStyle = { font: '30pt TheMinion', fill: 'white', align: 'center', stroke: 'rgba(0,0,0,0)', strokeThickness: 4};
    var taskStyle   = { font: '20pt TheMinion', fill: 'white', align: 'center', stroke: 'rgba(0,0,0,0)', strokeThickness: 4};
    var authorText  = game.add.text(game.world.centerX, 900, author, authorStyle);
    var taskText    = game.add.text(game.world.centerX, 950, task, taskStyle);
    authorText.anchor.setTo(0.5);
    //authorText.stroke = "rgba(0,0,0,0)";
    //authorText.stroke = "black";
    authorText.strokeThickness = 4;
    authorText.setShadow(3, 3, 'rgba(0,0,0,5)', 5);
    taskText.anchor.setTo(0.5);
    taskText.stroke = "rgba(0,0,0,0)";
    taskText.setShadow(3, 3, 'rgba(0,0,0,5)', 5);
    taskText.strokeThickness = 4;
    game.add.tween(authorText).to( { y: -300 }, 10000, Phaser.Easing.Cubic.Out, true, this.creditCount * 2500);
    game.add.tween(taskText).to( { y: -200 }, 10000, Phaser.Easing.Cubic.Out, true, this.creditCount * 2500);
    this.creditCount ++;
  },

  addMenuOption: function(text, callback) {
    var optionStyle = { font: '20pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    var txt = game.add.text(10, (this.optionCount * 80) + 450, text, optionStyle);

    txt.stroke = "rgba(0,0,0,0)";
    txt.strokeThickness = 4;   
    txt.setShadow(3, 3, 'rgba(0,0,0,5)', 5);
    var onOver = function (target) {
      //target.fill = "#FEFFD5";
      //target.stroke = "rgba(200,200,200,0.5)";
      target.stroke = "black";
      txt.useHandCursor = true;
    };
    var onOut = function (target) {
      target.fill = "white";
      target.stroke = "rgba(0,0,0,0)";
      txt.useHandCursor = false;
    };
    //txt.useHandCursor = true;
    txt.inputEnabled = true;
    txt.events.onInputUp.add(callback, this);
    txt.events.onInputOver.add(onOver, this);
    txt.events.onInputOut.add(onOut, this);

    this.optionCount ++;
  },

  create: function () {
    this.stage.disableVisibilityChange = true;
    
    var bg = game.add.sprite(0, 0, 'menu-bg');
    this.addCredit('Cyn'             , 'Inspired by');
    this.addCredit('Azucena Rosales' , 'Producer');
    this.addCredit('Azucena Rosales' , 'Programmer');
    this.addCredit('Azucena Rosales' , 'Designer');
    this.addCredit('Azucena Rosales' , 'Writer');
    this.addCredit('Azucena Rosales' , '2D-Artist');
    this.addCredit('Azucena Rosales' , 'Web Design');
    this.addCredit('Internet' , 'Images');
    this.addCredit('CTIN-eros'       , 'Beta Testers');            
    this.addCredit('Phaser.io'       , 'Powered By');
    this.addCredit('CYN <3'       , 'Happy Birthday');
    this.addMenuOption('<-', function (e) {
      game.state.start("GameMenu");
    });

    game.add.tween(bg).to({alpha: 0}, 10000, Phaser.Easing.Cubic.Out, true, 10000);
  }

};
