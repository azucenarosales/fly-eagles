var GameMenu = function() {};


GameMenu.prototype = {

  menuConfig: {
    startY: 260,
    startX: 30
  },

  init: function () {
    this.titleText = game.make.text(game.world.centerX, 100, "Fly Eagles!", {
      font:   'bold 40pt TheMinion',      
      fill:   'white',
      align:  'center'
      //fill: '#FDFFB5',
      //stroke: 'black'
    });
    this.titleText.setShadow(3, 3, 'rgba(0,0,0,5)', 5);
    this.titleText.anchor.set(0.5);
    this.optionCount = 1;
  },

  addMenuOption: function(text, callback) {
    var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    var txt = game.add.text(game.world.centerX, (this.optionCount * 80) + 380, text, optionStyle);
    txt.setShadow(3, 3, 'rgba(0,0,0,5)', 5);
    txt.anchor.setTo(0.5);
    txt.stroke = "rgba(0,0,0,0)";
    txt.strokeThickness = 4;

    var onOver = function (target) {
      target.fill       = "white";
      target.stroke     = "black";
      txt.useHandCursor = true;
    };
    var onOut = function (target) {
      target.fill       = "white";
      target.stroke     = "rgba(0,0,0,0)";
      txt.useHandCursor = false;
    };
    txt.inputEnabled = true;
    txt.events.onInputUp.add(callback, this);
    txt.events.onInputOver.add(onOver, this);
    txt.events.onInputOut.add(onOut, this);
    this.optionCount ++;
  },

  create: function () {    
    game.stage.disableVisibilityChange = true;
    game.add.sprite(0, 0, 'menu-bg');
    game.add.existing(this.titleText);

    this.addMenuOption('Jugar!', function () {
      game.state.start("Game");
    });

    // ------------------------
    // Flechitas
    // ------------------------
    var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    scoreText = game.add.text(game.world.centerX-35, 150, '↑↓ ', optionStyle);
    scoreText.setShadow(1, 1, 'rgba(0,0,0,5)', 5);

    this.addMenuOption('Creditos', function () {
      game.state.start("Credits");
    });
  }
};

//Phaser.Utils.mixinPrototype(GameMenu.prototype, mixins);