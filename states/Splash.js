var Splash = function () {};

Splash.prototype = {

  loadScripts: function () {
    game.load.script('WebFont', 'vendor/webfontloader.js');
    game.load.script('gamemenu','states/GameMenu.js');
    game.load.script('game', 'states/Game.js');
    game.load.script('gameover','states/GameOver.js');
    game.load.script('credits', 'states/Credits.js');
  },
  loadImages: function () {

            var playerData = [
            '.....CCCCCC.....',
            '...CCCCCCCCCC...',
            '.CCCCCCCCCCCCCC.',
            'CCC00000000000CC',
            'CC0222222222210C',
            'C0222222222210CC',
            '0222222211110CCC',
            '222222110000CCCC',
            '....0100CCCCCCCC',
            '....00CCCCCCCCCC',
            '....0CCCCCCCCCCC',
            '....0CCCCCCCCCCC',
            '....0CCCCCCCCCCC',
            '..0000CCC00CCCCC',
            '000.0.00C00CCCC.',
            '0.0.0000CCCCCC..',
            '00000.CCCCCCC...'        
        ];

        var cowboysData = [
            '.....222222.....',
            '...2211111122...',
            '..211111111112..',
            '.21111111111112.',
            '221111D111111112',
            '211111D111111111',
            '2111DDDDD11111..',
            '21111DDD111111..',
            '21111D1D1111....',
            '2111D111D111....',
            '211111111110....',
            '211111111110....',
            '211111111110....',
            '11111001100000..',
            '.111100100.0.000',
            '..1111110000.0.0',
            '...11111...00000'
        ];

        var ball = [
            '................',
            '.......55555555.',
            '......556666665.',
            '.....5566565565.',
            '....55666655665.',
            '...556656556565.',
            '..5566665566665.',
            '.55665655656665.',
            '.56666556666655.',
            '.5656556566655..',
            '.566556666655...',
            '.56556566655....',
            '.5656666655.....',
            '.566666655......',
            '.55555555.......',
            '................'
        ];

    game.create.texture('player', playerData, 3, 3, 0);        
    game.create.texture('cowboysTexture', cowboysData, 3, 3, 0);        
    game.create.texture('ball', ball, 3, 3, 0);

    game.load.image('menu-bg', 'assets/images/menu-bg.png');
    game.load.image('gameover-bg', 'assets/images/gameover-bg.png');
  },
  loadFonts: function () {
    WebFontConfig = {
      custom: {
        families: ['TheMinion'],
        urls: ['assets/style/theminion.css']
      }
    }
  },
  init: function () {
    this.loadingBar = game.make.sprite(game.world.centerX-(387/2), 500, "loading");
    this.status     = game.make.text(game.world.centerX-(387/2) +110, 450, 'Cargando...', {fill: 'white'});
  },
  preload: function () {
    game.add.sprite(0, 0, 'stars');
    game.add.existing(this.loadingBar);
    game.add.existing(this.status);
    this.load.setPreloadSprite(this.loadingBar);

    this.loadScripts();
    this.loadImages();
    this.loadFonts();
  },

  addGameStates: function () {
    game.state.add("GameMenu", GameMenu);
    game.state.add("Game", Game);
    game.state.add("GameOver", GameOver);
    game.state.add("Credits", Credits);
  },

  create: function() {
    this.addGameStates();
    setTimeout(function () {
      game.state.start("GameMenu");
    }, 1000);
  }
};
