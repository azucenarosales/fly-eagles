var Game = function(game) {};

var touchdownArea   = 400;
var largoCancha     = 6447;
var anchoCancha     = 600;
var bestScore       = 0;
var zonaValida      = 50;
var posession       = false;
var zonaPuntos      = largoCancha - touchdownArea*2;
var width   = 800;
var height  = 600;
var speed   = 450;
var score   = 0;
var player
var stars;
var cursors;
var scoreText;
var life = 3;

Game.prototype = {

    init: function () {
    /*this.loadingBar = game.make.sprite(game.world.centerX-(387/2), 400, "loading");
    this.logo       = game.make.sprite(game.world.centerX, 200, 'brand');
    this.status     = game.make.text(game.world.centerX, 380, 'Loading...', {fill: 'white'});
    utils.centerGameObjects([this.logo, this.status]);
*/

  },
    preload: function () {
        this.optionCount = 1;
        game.load.image('cuadro'     , 'assets/images/red-square.png');    
        game.load.image('background' , 'assets/images/stadium2.png');        
    },

    addMenuOption: function(text, callback) {
        var optionStyle = { 
            font:   '20pt TheMinion', 
            fill:   'white', 
            align:  'left', 
            stroke: 'rgba(0,0,0,0)', 
            srokeThickness: 4
        };
        var txt = game.add.text(0, 0, text, optionStyle);
        txt.fixedToCamera   = true;
        txt.strokeThickness = 4;
        txt.cameraOffset.setTo(650, 40);
        txt.anchor.setTo(0.5);
        txt.stroke = "rgba(0,0,0,0)";
        txt.setShadow(1, 1, 'rgba(0,0,0,5)', 5);
        
        var onOver = function (target) {          
          target.stroke     = "black";
          txt.useHandCursor = true;
          //target.fill = "#FEFFD5";
          //target.stroke = "rgba(200,200,200,0.5)";        
        };
        var onOut = function (target) {
          target.fill       = "white";
          target.stroke     = "rgba(0,0,0,0)";
          txt.useHandCursor = false;
        };
        txt.inputEnabled = true;
        txt.events.onInputUp.add(callback,  this);
        txt.events.onInputOver.add(onOver,  this);
        txt.events.onInputOut.add(onOut,    this);

        this.optionCount ++;
    },
    create: function () {        

        this.stage.disableVisibilityChange = true;
        game.add.tileSprite(0, 0, largoCancha, anchoCancha, 'background');
        game.world.setBounds(0, 0, largoCancha, anchoCancha);

        //start arcade physics engine
        game.physics.startSystem(Phaser.Physics.ARCADE);

        //inicializa las flechas del teclado como los controles
        cursors = game.input.keyboard.createCursorKeys();

        // ------------------------
        // Impulso inicial
        // ------------------------

        impulsoInicial = game.add.sprite(largoCancha - touchdownArea, game.world.height/2, 'cuadro');
        impulsoInicial.enableBody = true; 

        game.physics.enable(impulsoInicial, Phaser.Physics.ARCADE);

        // ------------------------
        // Ball
        // ------------------------
        ball = game.add.physicsGroup();
        //ball = ball.create((touchdownArea + 500), game.world.height/2, 'ball');
        ball = ball.create(largoCancha - touchdownArea - 300, game.world.height/2, 'ball');        
        ball.enableBody = true;   
        ball.body.collideWorldBounds = true;
        game.physics.enable(ball, Phaser.Physics.ARCADE);

        // ------------------------
        // Jugador
        // ------------------------
        player = game.add.physicsGroup();
        player = player.create(largoCancha - touchdownArea, game.world.height/2, 'player');
        player.body.bounce.x = 0.8;        
        player.body.collideWorldBounds = true;
        player.anchor.set(0.5);
        // Modo Facil:
        //player.body.bounce.x = 0.5;
        game.camera.follow(player);
        //  La camara sigue al jugador
        game.physics.arcade.enable(player);
        game.physics.arcade.overlap(player, impulsoInicial, impulso);

        // ------------------------
        // Enemigo
        // ------------------------

        cowboys = game.add.physicsGroup();

        var y = 10;

        for (var i = 0; i < 35; i++) {
            var cowboy = cowboys.create(game.world.randomX, y, 'cowboysTexture');
            cowboy.body.velocity.x = game.rnd.between(200, 400);        
            y += 20;        
            cowboy.body.immovable = true;
        }
    
        // ------------------------
        // Score
        // ------------------------
        var optionStyle = { font: '18pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
        scoreText = game.add.text(0, 0, 'SCORE: ' + score, optionStyle);
        scoreText.setShadow(1, 1, 'rgba(0,0,0,5)', 5);
        scoreText.fixedToCamera = true;
        scoreText.cameraOffset.setTo(20, 20);

        // ------------------------
        // Best Score
        // ------------------------

        bestScoreText = game.add.text(0, 0, 'Mejor Tiempo: ' + bestScore, optionStyle);
        bestScoreText.fixedToCamera = true;
        bestScoreText.cameraOffset.setTo(20, 60);

        // ------------------------
        // Vidas
        // ------------------------

        vidasText = game.add.text(0, 0, 'Vidas: ' + life, optionStyle);
        vidasText.fixedToCamera = true;
        vidasText.cameraOffset.setTo(20, 100);

        //-------------------------
        // Timer 
        // ------------------------
        timer = game.time.create(false);
        timer.loop(1000, updateCounter, this);
        timer.start();

        // -------------------------
        // Volver al Menu
        // -------------------------
        this.addMenuOption('Menu', function (e) {
            game.world.setBounds(0, 0, width, height);
            score = 0;
            game.state.start("GameMenu");
        });

    },

    update: function () {        
        cowboys.forEach(checkPos, this);
        player.body.velocity.y = 0;

        // Movimiento teclas arriba y abajo
        if (cursors.up.isDown) {
            player.body.velocity.y = -speed;
        } else if (cursors.down.isDown) {
            player.body.velocity.y = speed;
        }
        /*
        // Movimiento teclas derecha e izquierda
        if (cursors.left.isDown) {
            player.body.velocity.x = -speed;
        } else if (cursors.right.isDown) {
            player.body.velocity.x = speed;
        }*/

        // El jugador colisiona con la pelota
        game.physics.arcade.collide(player, ball, takeBall);   

        // El jugador toma la pelota
        if(posession) {
            ball.x = player.x;
            ball.y = player.y-15;
        }     

        // El jugador choca con el enemigo
        game.physics.arcade.collide(player, cowboys, colision);

        // Verifica que la pelota este en el area de anotacion
        if(ball.x < touchdownArea) {
            posession = true;
            if(player.x < touchdownArea - 100 && posession == true) {
                // Verifica si es la primera vez que se juega para poner ese tiempo como el mejor
                if (bestScore == 0) {
                    bestScore = score;
                    // Verifica que el tiempo se haya mejorado (sea menor)
                } else if(score < bestScore) {
                    bestScore = score; 
                }
                score = 0;
                life = 3;
                this.game.state.start("GameOver");
            } 
        }        
        
        vidasText.text = 'Vidas: ' + life;
        //----------------------------------
        // Incrementa los puntos por yardas
        //----------------------------------
       /* if(Math.round(player.x) > zonaPuntos && Math.round(player.x) < zonaPuntos + zonaValida ) {                
            score           = score + 10;
            zonaPuntos      = zonaPuntos - touchdownArea;
            scoreText.text  = "Score: " + score;
        }*/
        //scoreText.text = 'player x' + Math.round(player.x) + '\ntouchdownArea ' + touchdownArea + '\nzonaPuntos ' + zonaPuntos  + '\nscore: ' +  score;
    },
    render: function() {
        scoreText.text = 'Tiempo: ' + score;
    }
};

function updateCounter() {
    score++;
}

function takeBall(player, ball) {
    player.body.velocity.x = -200;    
    posession = true;
}

function checkPos (cowboy) {
    if (cowboy.x > largoCancha) {
        cowboy.x = -100;
    }    
}

function colision (player, cowboy) {
    cowboy.kill();
    if(player.body.touching.left == true) {
        life = --life;
    }

    if(player.body.touching.right == true){
        // El player toca al cowboy del lado derecho
        player.body.velocity.x = -300;
    }

    if(player.body.touching.up == true){
        // El player toca al cowboy del lado derecho
        player.body.velocity.x = -300;
    }

    if(player.body.touching.down == true){
        // El player toca al cowboy del lado derecho
        player.body.velocity.x = -300;
    }

    if(life < 0) {
            player.x = largoCancha - touchdownArea;
            player.y = game.world.height/2;
            life = 3;

            ball.x = largoCancha - touchdownArea - 300;
            ball.y = Math.random() * (game.world.height - 0) + 0;

            posession = false;

        }
}

function impulso(player, obj) {
   player.body.velocity.x = -200;
    obj.kill();
}

function fnScore() {
    score++;
    scoreText.text = scoreText.text = "Score: " + score;
}