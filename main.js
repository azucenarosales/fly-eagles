// Global Variables
var
  game = new Phaser.Game(800, 600, Phaser.CANVAS, 'game'),
  Main = function () {};

Main.prototype = {
  preload: function () {
    game.load.image('stars',    'assets/images/stars.png');
    game.load.image('loading',  'assets/images/loading.png');
    game.load.script('splash',  'states/Splash.js');
  },

  create: function () {
    game.state.add('Splash', Splash);
    game.state.start('Splash');
  }

};

game.state.add('Main', Main);
game.state.start('Main');
